/* 
    gen-mouse-ev.c: Generate mouse button events.
    
    Ref.: fakeMouse.c (https://bharathisubramanian.wordpress.com/2010/04/01/x11-fake-mouse-events-generation-using-xtest/)
    2018/12/13 butty
    
    usage: gen-mouse-ev [mouse-button-syms...]
        mouse-button-syms: 1, 2, 3, ...; Sn: sleep n(ms)
        
        If no args specified, mouse-button-syms are obtained from stdin.     
            eg. echo -e "4\nS200\n5" | ./gen-mouse-ev
*/

/*
Copyright 2018-2020 Butty PiuLento

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
Compilation:
    gcc gen-mouse-ev.c -o gen-mouse-ev -lX11 -lXtst -lXext
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <X11/extensions/XTest.h>

// #define DEBUG

#define ENABLE_READ_STDIN // Read button syms from stdin when no args specified.
/*
#define BUTTON_HOLD_TIME_US (20000) // Button hold (pressing) time (us).
#define BUTTON_INT_US (20000) // Internval between buttons (us).
*/
/*
#define BUTTON_HOLD_TIME_US (12000) // Button hold (pressing) time (us).
#define BUTTON_INT_US (12000) // Internval between buttons (us).
*/
#define BUTTON_HOLD_TIME_US (10000) // Button hold (pressing) time (us).
#define BUTTON_INT_US (10000) // Internval between buttons (us).
/*
#define BUTTON_HOLD_TIME_US (100) // Button hold (pressing) time (us).
#define BUTTON_INT_US (100) // Internval between buttons (us).
*/

#define BUTTON_PSYM_SLEEP 'S' // Pseudo button sym. of sleep.
#define BUTTON_PSYM_SYNC 'C' // Pseudo button sym. of XSync (an empty line (newline) can also be used).

int 
main(int ac, char **av)
{
    char *myname= av[0];
    Display *dpy= NULL;
    int read_stdin= 0;
  
    if (ac < 2) {
#ifdef ENABLE_READ_STDIN
        read_stdin= 1;
#else // !ENABLE_READ_STDIN
        printf("usage: %s [mouse-button-syms...]\n", myname);
        
        exit(1);
#endif // !ENABLE_READ_STDIN
    }
    
#ifdef DEBUG
    printf("%s: Opening display...\n", myname);
#endif // DEBUG
    dpy= XOpenDisplay(NULL);
    if (!dpy) {
        printf("%s: Error: XOpenDisplay() failed.\n", myname);
      
        exit(-1);
    }

    {
#ifdef DEBUG
        printf("%s: Generating mouse button events...\n", myname);
#endif // DEBUG 
        if (!read_stdin) {
            av++;
        }
        
        while (read_stdin || *av) {
            unsigned int button_num;
            char *button_sym;
            char button_buf[BUFSIZ];
            double sleep_ms_f;
            useconds_t sleep_us;
            
            if (read_stdin) {
                int s;
                char *p;
                
                if (isatty(fileno(stdin))) {
                    printf("Enter button sym ('Sf': Sleep f ms, 'C' or empty: Do XSync): "); fflush(stdout);
                }
                
                p= fgets(button_buf, sizeof(button_buf), stdin);
                if (!p || feof(stdin)) {
#ifdef DEBUG
                    printf("[EOF]\n");
#endif // DEBUG                     
                    break;
                }
                
                // Remove newline.
                p= strchr(button_buf, '\n');
                if (*p) {
                    *p= '\0';
                }
                
                if (strlen(button_buf) == 0) {
                    sprintf(button_buf, "%c", BUTTON_PSYM_SYNC); // Perform XSync.
                    // continue;
                }
                
                button_sym= button_buf;
            } else {
                button_sym= *av;
                
                av++;
            }
            
            if (*button_sym == BUTTON_PSYM_SLEEP) {
                sleep_ms_f= atof(button_sym+1);
                sleep_us= (useconds_t)(sleep_ms_f*1000.0);
                // sleep_us= (useconds_t)atoi(button_sym+1);               
                  
#ifdef DEBUG
                    printf("%s: sleep_us=%u\n", myname, (unsigned int)sleep_us);
#endif // DEBUG 
                usleep(sleep_us);
            } else if (*button_sym == BUTTON_PSYM_SYNC) {
#ifdef DEBUG
                printf("%s: Performing XSync...\n", myname);
#endif // DEBUG  
                XSync(dpy, False);
            } else {
                button_num= atoi(button_sym);
#ifdef DEBUG
                printf("%s: Generating mouse button %d...\n", myname, button_num);
#endif // DEBUG
            
                XTestFakeButtonEvent(dpy, button_num, True/* Button down */,  CurrentTime); 
#ifdef BUTTON_HOLD_TIME_US
                usleep(BUTTON_HOLD_TIME_US);
#endif // BUTTON_HOLD_TIME_US
                XTestFakeButtonEvent(dpy, button_num, False/* Button up */, CurrentTime);
                
                /*
                if (read_stdin) {
#ifdef DEBUG
                    printf("%s: Performing XSync...\n", myname);
#endif // DEBUG  
                    XSync(dpy, False); // Necessary.
                }
                */
                
#ifdef BUTTON_INT_US
                usleep(BUTTON_INT_US);
#endif // BUTTON_INT_US
            }
        } // while
#ifdef DEBUG
        printf("%s: Done.\n", myname);
#endif // DEBUG  
    }

    // if (!read_stdin) {
#ifdef DEBUG
        printf("%s: Performing XSync...\n", myname);
#endif // DEBUG  

        XSync(dpy, False);
    // }
    
#ifdef DEBUG
    printf("%s: Closing display...\n", myname);
#endif // DEBUG  
    XCloseDisplay(dpy);

#ifdef DEBUG
    printf("%s: Exiting.\n", myname);
#endif // DEBUG
  
    exit(0);
}