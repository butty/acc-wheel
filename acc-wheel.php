<?php
/* 
    Mouse wheel accelerator.
    2018/12/12,13- butty

    usage: acc-wheel.php [-h] [-d[v]] [-v|-q] [-xdt] [-m S|L|SL] [mouse-ev-dev-name]
*/

/*
Copyright 2018-2020 Butty PiuLento

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

define("DEBUG", FALSE);
if (DEBUG) {
    // define("$debug_v_mode", TRUE); // More debug msgs (very verbose).
    define("DEBUG_V", FALSE); // More debug msgs.
} else {
    define("DEBUG_V", FALSE); // More debug msgs.
}    
define("VERBOSE", TRUE);

define("PROG_NAME", "acc-wheel");
define("PROG_VERSION", "0.2.1"); // Program version.

define("DO_LOCK", TRUE); // Avoid mult. running.

define("ENABLE_IC_APPS_WA", TRUE); // Enable workarounds for some incompat. apps (eg. NixNote2).

/*
// Note: Unfortunately, xdotool doesn't work as expected and USE_GEN_MOUSE_EV_CMD must be TRUE.
// define("USE_GEN_MOUSE_EV_CMD", TRUE); // TRUE: Use gen-mouse-ev command instead of xdotool to gen mouse events.
// define("USE_GEN_MOUSE_EV_CMD", FALSE); // FALSE: Use xdotool instead of gen-mouse-ev command to gen mouse events.
*/

$myname= $argv[0];

$debug_mode= DEBUG;
$debug_v_mode= DEBUG_V;
$verbose_mode= VERBOSE;

// Obtain user name and home dir.
{
    $uid= posix_getuid();
    $pw_info= posix_getpwuid($uid);
    $uname= $pw_info["name"];
    $home_dir= $pw_info["dir"];
}

if (DO_LOCK) {
    $tmp_env= getenv("XDG_RUNTIME_DIR");
    if ($tmp_env) {
        $tmp_dir= $tmp_env;
    } else {      
        $tmp_dir= "/tmp";
    }
    define("LOCK_FILE_BASENAME", $tmp_dir."/".PROG_NAME);
}

define("RESTART_WAIT_S", 5); // Sleep time (s) before restarting.

// gen-mouse-ev's config.
// if (USE_GEN_MOUSE_EV_CMD) 
{ 
    define("GEN_MOUSE_EV_CMD_BASENAME", "gen-mouse-ev");
    
    //// Workaround for NixNote2's scroll malfunction (incomplete).
    define("GEN_MOUSE_EV_CMD_PRE_XSYNC", FALSE); // Perform XSync before generating add. events.

    // define("GEN_MOUSE_EV_CMD_PRE_SLEEP_US", 5*1000); // Sleep time (us) before generating add. events.
    define("GEN_MOUSE_EV_CMD_PRE_SLEEP_US", FALSE); // Sleep time (us) before generating add. events.
    
//    define("GEN_MOUSE_EV_CMD_XSYNC_EV_INT", 1); // XSync interval (every wheel events).
    define("GEN_MOUSE_EV_CMD_XSYNC_EV_INT", 2); // XSync interval (every wheel events).
    // define("GEN_MOUSE_EV_CMD_XSYNC_EV_INT", 4); // XSync interval (every wheel events).
//    define("GEN_MOUSE_EV_CMD_XSYNC_EV_INT", 10); // XSync interval (every wheel events).

    define("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS", 1); // Sleep time after issued an XSync (ms). Floating point vals. (eg. 0.1) can be used.
    // define("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS", 5); // Sleep time after issued an XSync (ms). Floating point vals. (eg. 0.1) can be used.
//    define("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS", 10); // Sleep time after issued an XSync (ms). Floating point vals. (eg. 0.1) can be used.
//    define("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS", 25); // Sleep time after issued an XSync (ms). Floating point vals. (eg. 0.1) can be used.
    ////    

    /*
    // gen-mouse-ev command's search paths (and GEN_MOUSE_EV_CMD_BASENAME will be checked, too).
    $gen_mouse_ev_cmd_cands= array(__DIR__."/gen-mouse-ev/".GEN_MOUSE_EV_CMD_BASENAME, __DIR__."/".GEN_MOUSE_EV_CMD_BASENAME, $home_dir."/bin/".GEN_MOUSE_EV_CMD_BASENAME);
    */
}

// xdotool's config.
// if (!USE_GEN_MOUSE_EV_CMD) 
{
    define("XDOTOOL_CMD", "xdotool");
//    define("XDOTOOL_CMD", "/usr/bin/xdotool");
    
    // Note: It seems that xdotool does not execute commands read from stdin before it is closed. So, USE_XDOTOOL_VIA_PIPE and USE_XDOTOOL_VIA_FIFO do not work as expected.
    define("USE_XDOTOOL_VIA_PIPE", FALSE); // [Do not enable] Use xdotool via pipe.: Note: Somehow, xdotool does not work when reading commands from stdin.
    define("USE_XDOTOOL_VIA_FIFO", FALSE); // Use fifo (named pipe).: Does not work as pipe, too.

    define("XDOTOOL_NO_REPEAT", FALSE); // [USE_XDOTOOL_VIA_PIPE] Do not use --repeat to clilck.
    
    define("XDOTOOL_ISSUE_EV_SPECIFY_WIN", FALSE); // [Don't enable] [xdotool (= !$use_gen_mouse_ev_cmd)] Specify the window when issuing the wheel event (clicking) on xdotool.: Not good.: When enabled, XTest is not used to gen. events, added wheel events will be ignored by some apps. 
}

//// Mouse event defs.
define("EVDEV_EV_PKT_LEN", 24); // Evdev's event packet len. (B)
/* Packet structuere. (Ref.: http://who-t.blogspot.com/2016/09/understanding-evdev.html)
struct input_event {
    struct timeval time; // 16B
    __u16 type; // 2B 
    __u16 code; // 2B 
    __s32 value; // 4B
};

Sample data from /dev/input/eventN:
b5 91 10 5c 00 00 00 00 ed 52 00 00 00 00 00 00; 02 00; 08 00; ff ff ff ff #  Event type 2 (EV_REL); Event code 8 (REL_WHEEL); Val= 0xffffffff (-1)
b5 91 10 5c 00 00 00 00 ed 52 00 00 00 00 00 00; 00 00; 00 00; 00 00 00 00 # Event type 0 (EV_SYN); Event code 0 (SYN_REPORT); Val= 0  

b5 91 10 5c 00 00 00 00 22 e9 01 00 00 00 00 00; 02 00; 08 00; ff ff ff ff #  Event type 2 (EV_REL); Event code 8 (REL_WHEEL); Val= 0xffffffff (-1)
b5 91 10 5c 00 00 00 00 22 e9 01 00 00 00 00 00; 00 00; 00 00; 00 00 00 00  

b7 91 10 5c 00 00 00 00 1b c1 0d 00 00 00 00 00; 02 00; 08 00; 01 00 00 00 #  Event type 2 (EV_REL); Event code 8 (REL_WHEEL); Val= 1
b7 91 10 5c 00 00 00 00 1b c1 0d 00 00 00 00 00; 00 00; 00 00; 00 00 00 00  

b8 91 10 5c 00 00 00 00 c8 8e 01 00 00 00 00 00; 02 00; 08 00; 01 00 00 00 #  Event type 2 (EV_REL); Event code 8 (REL_WHEEL); Val= 1  
b8 91 10 5c 00 00 00 00 c8 8e 01 00 00 00 00 00; 00 00 00; 00 00 00 00 00  
*/
define("EV_TYPE_REL", 2); // Event type: "Rel".
define("EV_CODE_REL_WHEEL", 8); // Event code: "Wheel" (type= Rel).
define("EV_VAL_WHEEL_DOWN", 0xffffffff); // Event value: "Wheel down".
define("EV_VAL_WHEEL_UP", 1); // Event value: "Wheel up".

define("EV_TYPE_SYN", 0); // Event type: "Syn".
define("EV_CODE_SYN_SYN", 0); // Event code: "Syn" (type= Syn).
////

//// Event recv. config.
// define("EV_WAIT_TIME_MAX_MS", 25); // Max. event wait time (ms).
define("EV_WAIT_TIME_MAX_MS", 50); // Max. event wait time (ms).

//// Wheel accel. config.
// define("WHEEL_AGGR_ACCEL", FALSE); // Aggregate issuing additional events.
define("WHEEL_AGGR_ACCEL", TRUE); // Aggregate issuing additional events.

if (WHEEL_AGGR_ACCEL) { // [WHEEL_AGGR_ACCEL] Max. aggregating event period (ms).
    // define("WHEEL_AGGR_ACCEL_MAX_MS", 5);

    define("WHEEL_AGGR_ACCEL_MAX_MS", 7);

    // define("WHEEL_AGGR_ACCEL_MAX_MS", 15);
    // define("WHEEL_AGGR_ACCEL_MAX_MS", 30);
}

// define("WHEEL_ACCEL_DT_MS", 50); // [NOT USED] Period to check if acceleration to be applied (ms).

//    define("DEF_WHEEL_ACCEL_MODE", "L"); // Wheel accel. mode: "LS", "S", "L".
define("DEF_WHEEL_ACCEL_MODE", "S"); // Wheel accel. mode: "LS", "S", "L".
//    define("DEF_WHEEL_ACCEL_MODE", "LS"); // Wheel accel. mode: "LS", "S", "L".

/*
// Tweeks for some incompat. apps (eg. NixNote2).
if (USE_GEN_MOUSE_EV_CMD) {
    //    define("WHEEL_EV_ADD_INT_MS", 2); // Internval of issuing added wheel events (ms). Zero or floating point vals. (eg. 0.1) can be used.
       define("WHEEL_EV_ADD_INT_MS", 2.5);
    //    define("WHEEL_EV_ADD_INT_MS", 3);
    //    define("WHEEL_EV_ADD_INT_MS", 5);
//    define("WHEEL_EV_ADD_INT_MS", 10);
    //    define("WHEEL_EV_ADD_INT_MS", 15);
} else { // xdotool
//        define("WHEEL_EV_ADD_INT_MS", 3);
        define("WHEEL_EV_ADD_INT_MS", 30);
}
*/

define("DEF_WHEEL_EV_ADD_MAX", 10); /* [Just in case] Max. num of added events (to avoid too many events) */
// define("DEF_WHEEL_EV_ADD_MAX", 15);

// Wheel acc. config(params) of each acc-mode.
$wheel_acc_configs= array(
    // Step accel. mode.
    /*
        Added events= (WHEEL_ACCEL_VW_MULT * k * (Orig. events)) - (Orig. events).
            k= 1 (when vw < WHEEL_ACCEL_VW_TH),
                WHEEL_ACCEL_A * vw/WHEEL_ACCEL_VW_TH (when vw >= WHEEL_ACCEL_VW_TH)
            
            where vw is velocity(speed) of wheel event.
    */
    "S"=>array(
        "WHEEL_ACCEL_VW_MULT"=>1.5 /* Mult. factor of wheel events when wheel is fast. */,
        "WHEEL_ACCEL_VW_TH"=>(WHEEL_AGGR_ACCEL?0.08:0.13) /* Threshold velocity (ev/ms) to increase the mult. factor. */,
        "WHEEL_ACCEL_A"=>(WHEEL_AGGR_ACCEL?2:1.5) /* Mult. factor of wheel events when wheel is faster (vw >= WHEEL_ACCEL_VW_TH) */, 
        "WHEEL_EV_ADD_MAX"=>DEF_WHEEL_EV_ADD_MAX /* [Just in case] Max. num of added events (to avoid too many events) */,
        
        // "WHEEL_EV_ADD_INT_MS"=>WHEEL_EV_ADD_INT_MS /* Internval of issuing added wheel events (ms). Zero or floating point vals. (eg. 0.1) can be used. */
        ),
    
    // Linear accel. mode.
    /*
        Added events= (WHEEL_ACCEL_VW_MULT * vw * (Orig. events)) - (Orig. events).
        
        where vw is velocity(speed) of wheel event.
    */
    "L"=>array(
        "WHEEL_ACCEL_VW_MIN"=>0.03 /* Min. velocity (ev/ms) to start accel. (issue add. events).*/, 
        "WHEEL_ACCEL_VW_SUB"=>0 /* Subtracted val. form vw. */,
        "WHEEL_ACCEL_VW_MULT"=>20 /* Mult. factor of wheel events. */,
//        "WHEEL_ACCEL_VW_MULT"=>(USE_GEN_MOUSE_EV_CMD ? 20:40) /* Mult. factor of wheel events. */,
        "WHEEL_ACCEL_MULT_OFF"=>0 /* Offset val. to add. events. */, 
        "WHEEL_EV_ADD_MAX"=>DEF_WHEEL_EV_ADD_MAX /* [Just in case] Max. num of added events (to avoid too many events) */,
        
        // "WHEEL_EV_ADD_INT_MS"=>WHEEL_EV_ADD_INT_MS /* Internval of issuing added wheel events (ms). Zero or floating point vals. (eg. 0.1) can be used. */
        ),
    
    // Slower linear accel. mode.
    /*
        Added events= (WHEEL_ACCEL_VW_MULT * (vw - WHEEL_ACCEL_VW_SUB) * (Orig. events)) + ((Orig. events)*WHEEL_ACCEL_MULT_OFF) - (Orig. events).
        = (WHEEL_ACCEL_VW_MULT * (vw - WHEEL_ACCEL_VW_SUB) + WHEEL_ACCEL_MULT_OFF) * (Orig. events)
        
        where vw is velocity(speed) of wheel event.
    */
    "LS"=>array(
        "WHEEL_ACCEL_VW_MIN"=>0.025,
        "WHEEL_ACCEL_VW_SUB"=>0.025, 
        "WHEEL_ACCEL_VW_MULT"=>4, 
        "WHEEL_ACCEL_MULT_OFF"=>1.75, 
        "WHEEL_EV_ADD_MAX"=>10,
        
        // "WHEEL_EV_ADD_INT_MS"=>WHEEL_EV_ADD_INT_MS
        ),   
);

define("WHEEL_FLUSH_EV_EC", FALSE); // Flush wheel events on event change.
// define("WHEEL_ACCEL_DELAY_MS", 5); // [Not so useful] Wait time before start issuing add. events (ms).
//// 

// Signal handler.
// Note: Not called during stream_select().
function sig_handler()
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;
    
    echo $myname.": sig_handler: called.\n";
    
    cleanup();
    
    echo $myname.": exiting.\n";

    exit(0);
}

// Set signal handler.
$s= pcntl_signal(SIGTERM, "sig_handler");
if (!$s) {
    echo $myname.": Error: pcntl_signal(SIGTERM) failed.\n";
}
$s= pcntl_signal(SIGINT, "sig_handler");
if (!$s) {
    echo $myname.": Error: pcntl_signal(SIGINT) failed.\n";
}
$s= pcntl_signal(SIGHUP, "sig_handler");
if (!$s) {
    echo $myname.": Error: pcntl_signal(SIGHUP) failed.\n";
}
   
//// Perform exclusive locking.
$lock_fp= FALSE;
if (DO_LOCK) {
    $lock_file= LOCK_FILE_BASENAME."-".$uname.".lock";
    
    if ($debug_mode) {
        echo $myname.": Locking (lock_file=".$lock_file.")...\n";
    }

    $lock_fp= fopen($lock_file, "a+");
    if (!$lock_fp) {
        echo $myname.": Error: Failed to open lock file: ".$lock_file.".\n";
        
        exit(1);
    }
    
    $s= flock($lock_fp, LOCK_EX | LOCK_NB);
    if (!$s) {
        echo $myname.": Error: flock() failed (file=".$lock_file."), another process already running, exiting.\n";
        
        fclose($lock_fp);
        $lock_fp= FALSE;
        
        exit(1);
    }
    
    if ($debug_mode) {
        echo $myname.": Lock succeeded.\n";
    }
}

function cleanup()
{   
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;
    global $lock_fp, $lock_file;
    global $use_gen_mouse_ev_cmd;
    global $gen_mouse_ev_cmd_pipe;
    global $xdt_fifo_name;
   
    if ($debug_mode || $verbose_mode) {
        echo $myname.": cleanup...\n";
    }
    
    if ($lock_fp) {
        flock($lock_fp, LOCK_UN);
        fclose($lock_fp);
        $lock_fp= FALSE;
        
        @unlink($lock_file);
    }
    
    if ($gen_mouse_ev_cmd_pipe) { // Close the pipe and terminate the proc.
        if ($verbose_mode || $debug_mode) {
            echo $myname.": Closing the pipe to ".($use_gen_mouse_ev_cmd ? GEN_MOUSE_EV_CMD_BASENAME:XDOTOOL_CMD)."...\n";
        }
        if (!$use_gen_mouse_ev_cmd && USE_XDOTOOL_VIA_FIFO) {
            fclose($gen_mouse_ev_cmd_pipe);
            @unlink($xdt_fifo_name);
        } else {
            pclose($gen_mouse_ev_cmd_pipe);
        }
        $gen_mouse_ev_cmd_pipe= FALSE;
    }
}

function usage() {
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;
    
    echo "usage: php ".$myname." [-h] [-d[v]] [-v|-q] [-xdt] [-m S|L|SL] [mouse-ev-dev-name]\n";
}

// Main
{
    $use_gen_mouse_ev_cmd= TRUE;
    $wheel_acc_mode= DEF_WHEEL_ACCEL_MODE;
    $mouse_dev= $mouse_dev_arg= FALSE;
    
    // Parse args.
    for ($arg_idx= 1; $arg_idx < $argc && $argv[$arg_idx]; $arg_idx++) {
        if ($debug_mode) {
            echo $arg_idx.": argv=".$argv[$arg_idx]."\n";
        }
        
        switch ($argv[$arg_idx]) {
            case "-d":
                $debug_mode= TRUE;

                if ($debug_mode) {
                    echo $arg_idx.": debug_mode.\n";
                }        
                break;

            case "-dv":
                $debug_v_mode= TRUE;

                if ($debug_mode) {
                    echo $arg_idx.": debug_v_mode.\n";
                }        
                break;
                
            case "-v":
                $verbose_mode= TRUE;

                if ($debug_mode) {
                    echo $arg_idx.": verbose_mode.\n";
                }        
                break;

            case "-q":
                $verbose_mode= FALSE;

                if ($debug_mode) {
                    echo $arg_idx.": quiet mode.\n";
                }        
                break;

            case "-xdt": // Note: xdotool doesn't work as expected (ie. read cmds. from stdin) and CPU load will be higher in this mode.
                $use_gen_mouse_ev_cmd= FALSE;

                if ($verbose_mode || $debug_mode) {
                    echo $arg_idx.": Using xdotool.\n";
                }        
                break;
                                
            case "-m":
                if (isset($argv[$arg_idx+1])) {
                    $arg_idx++;
                    $wheel_acc_mode= $argv[$arg_idx];
                    if ($debug_mode) {
                        echo $arg_idx.": wheel_acc_mode=".$wheel_acc_mode."\n";
                    }        
                    
                    // Check the mode.
                    if (!isset($wheel_acc_configs[$wheel_acc_mode])) {                    
                    // if ($wheel_acc_mode != "LS" && $wheel_acc_mode != "L" && $wheel_acc_mode != "S") {
                        echo $myname.": Error: Invalid acc-mode: ".$wheel_acc_mode.", default (".DEF_WHEEL_ACCEL_MODE.") used.\n";
                        
                        $wheel_acc_mode= DEF_WHEEL_ACCEL_MODE;

                        /*
                        echo $myname.": Error: Invalid acc-mode: ".$wheel_acc_mode.".\n";
                        usage();
                        
                        exit(1);               
                        */
                    }
                } else {
                    echo $myname.": Error: Acc-mode must be specified to -m.\n";
                    usage();
                    
                    exit(1);
                }
                break;
                
            case "-h":
//            case "--help":
                usage();
                
                exit(0);
                break;                
                
            default: // Device name.
                $mouse_dev_arg= $argv[$arg_idx];
                goto END_OPTS;
                
                break;
        }
    }
END_OPTS:
    
    if ($use_gen_mouse_ev_cmd) { // Search gen-mouse-ev command and init.
        // gen-mouse-ev command's search paths (and GEN_MOUSE_EV_CMD_BASENAME will be checked, too).
        $gen_mouse_ev_cmd_cands= array(__DIR__."/gen-mouse-ev/".GEN_MOUSE_EV_CMD_BASENAME, __DIR__."/".GEN_MOUSE_EV_CMD_BASENAME, $home_dir."/bin/".GEN_MOUSE_EV_CMD_BASENAME);
        
        $gen_mouse_ev_cmd= FALSE;
        
        $gen_mouse_ev_cmd= trim(shell_exec("which ".GEN_MOUSE_EV_CMD_BASENAME));
        if ($debug_mode) {
            echo $myname.": which ".GEN_MOUSE_EV_CMD_BASENAME.": res=".$gen_mouse_ev_cmd."\n";
        }
        
        if (!$gen_mouse_ev_cmd) {
            foreach ($gen_mouse_ev_cmd_cands as $cmd_path) {
                if (is_executable($cmd_path)) {
                    $gen_mouse_ev_cmd= $cmd_path;

                    if ($debug_mode) {
                        echo $myname.": Found: gen_mouse_ev_cmd=".$gen_mouse_ev_cmd."\n";
                    }                    
                    
                    break;
                }
            }
        }
        
        // $gen_mouse_ev_cmd= FALSE; // for test
        
        if ($debug_mode) {
            echo $myname.": gen_mouse_ev_cmd=".$gen_mouse_ev_cmd."\n";
        }
                
        if (!$gen_mouse_ev_cmd) {
            if ($debug_mode || $verbose_mode) {
                echo $myname.": Warning: ".GEN_MOUSE_EV_CMD_BASENAME." not found or not executable, using ".XDOTOOL_CMD." instead.\n";
            }
            
            $use_gen_mouse_ev_cmd= FALSE;
        }
    }

    if ($debug_mode) {
        echo  $myname.": argc=".$argc.", arg_idx=".$arg_idx.", use_gen_mouse_ev_cmd=".($use_gen_mouse_ev_cmd?"Y":"N").", wheel_acc_mode=".$wheel_acc_mode.", mouse_dev_arg='".$mouse_dev_arg."'.\n";

        echo $myname.": wheel acc. config=".print_r($wheel_acc_configs[$wheel_acc_mode], TRUE)."\n";
    }

    // gen-mouse-ev or xdotool specific init.
    if ($use_gen_mouse_ev_cmd) { // gen-mouse-ev
        // $wheel_acc_configs["L"]["WHEEL_ACCEL_VW_MULT"]= 20;
        
        if (ENABLE_IC_APPS_WA) { // Tweeks for some incompat. apps (eg. NixNote2).
            // define("WHEEL_EV_ADD_INT_MS", 1);
            
            // define("WHEEL_EV_ADD_INT_MS", 2.5);           
            define("WHEEL_EV_ADD_INT_MS", 3);
            
            // define("WHEEL_EV_ADD_INT_MS", 4);            
             // define("WHEEL_EV_ADD_INT_MS", 10);
            // define("WHEEL_EV_ADD_INT_MS", 20);
        }
    } else { // xdotool        
        $wheel_acc_configs["L"]["WHEEL_ACCEL_VW_MULT"]= 40;

        if (ENABLE_IC_APPS_WA) { // Tweeks for some incompat. apps (eg. NixNote2).
            define("WHEEL_EV_ADD_INT_MS", 30);
        }
    }    
    
    // exit(0);
    
	// Start and create the pipe to gen-mouse-ev proc.
	$gen_mouse_ev_cmd_pipe= FALSE;
    if ($use_gen_mouse_ev_cmd) {
        if (!$gen_mouse_ev_cmd) { // Should never happen. Just in case.
            echo $myname.": Error: ".GEN_MOUSE_EV_CMD_BASENAME." not found or not executable.\n";
            
            exit(1);
        }
        
        if ($debug_mode) {
            echo $myname.": Opening pipe to ".GEN_MOUSE_EV_CMD_BASENAME."...\n";
        }                            
        $gen_mouse_ev_cmd_pipe= popen($gen_mouse_ev_cmd, "w");
        if (!$gen_mouse_ev_cmd_pipe) {
            echo $myname.": Error: popen(".$gen_mouse_ev_cmd.") failed, using xdotool.\n";
        } else {
            if ($verbose_mode || $debug_mode) {
                echo $myname.": Successfully created the pipe to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
            }                    
        }
    } else { // xdotool
        if (USE_XDOTOOL_VIA_FIFO) {
            $xdt_fifo_name= "/tmp/aw-xdt-".$uname.".fifo"; // TODO
            
            if (file_exists($xdt_fifo_name)) {
                @unlink($xdt_fifo_name);
            }
            
            $cmd= "mkfifo ".$xdt_fifo_name;
            if (TRUE || $debug_mode) {
                echo $myname.": Making named pipe ".$xdt_fifo_name." for ".XDOTOOL_CMD." (cmd=".$cmd.")...\n";
            }
            
            $ret_str= system($cmd, $ret);
            if ($ret != 0) {
                echo $myname.": Error: Make named pipe for ".XDOTOOL_CMD." (cmd=".$cmd.") failed (ret=".$ret.", ret_str=".$ret_str.").\n";
                
                // TODO
                
                exit(1);
            } else {
                if (TRUE || $debug_mode) {
                    echo $myname.": Successfully made named pipe for ".XDOTOOL_CMD.".\n";
                }
                
                $cmd= XDOTOOL_CMD." ".$xdt_fifo_name." < /dev/null > /dev/null 2>&1 &";
                $ret_str= system($cmd, $ret);
                if ($ret != 0) {
                    echo $myname.": Error: Failed to start ".XDOTOOL_CMD."  (ret=".$ret.", ret_str=".$ret_str.", cmd=".$cmd.").\n";
                    
                    // TODO
                    
                    exit(1);
                } else {            
                    if ($verbose_mode || $debug_mode) {
                        echo $myname.": Successfully started ".XDOTOOL_CMD.".\n";
                    }                    

                    $gen_mouse_ev_cmd_pipe= fopen($xdt_fifo_name, "w");
                    if (!$gen_mouse_ev_cmd_pipe) {
                        echo $myname.": Error: Failed to open named pipe ".$xdt_fifo_name." for ".XDOTOOL_CMD.".\n";
    
                        // TODO
                    
                        exit(1);
                    } else {
                        if ($verbose_mode || $debug_mode) {
                            echo $myname.": Successfully opened named pipe for ".XDOTOOL_CMD.".\n";
                        }                    
                    }
                }
            }        
        } else if (USE_XDOTOOL_VIA_PIPE) { // Use xdotool via pipe.
            $xdotool_cmd= XDOTOOL_CMD." -";            
            // $xdotool_cmd= "exec ".XDOTOOL_CMD." -";            
            if ($debug_mode) {
                // $xdotool_cmd= "cat | ".XDOTOOL_CMD." -";
                // $xdotool_cmd= "tee /tmp/aw-xdt-cmds.txt | ".XDOTOOL_CMD." -";
                // $xdotool_cmd.= " > /tmp/aw-xdt.log 2>&1";
                
                echo $myname.": Opening pipe to ".XDOTOOL_CMD." (cmd=".$xdotool_cmd.")...\n";
            }                            
               
            $gen_mouse_ev_cmd_pipe= popen($xdotool_cmd, "w");
            if (FALSE) { // Trying with proc_open (same result as popen).
                $desc_spec= array(
                    0=>array("pipe", "r") // stdin of child.
                );
                $gen_mouse_ev_cmd_pipes= FALSE;
                $xdt_proc= proc_open($xdotool_cmd, $desc_spec , $gen_mouse_ev_cmd_pipes);
                
                if (!is_resource($xdt_proc)) {
                    echo $myname.": Error: proc_open(".$xdotool_cmd.") failed.\n";
                    
                    // TODO
                    
                    exit(1);
                } else {
                    if ($debug_mode) {
                        echo $myname.": gen_mouse_ev_cmd_pipes=".print_r($gen_mouse_ev_cmd_pipes, TRUE).".\n";
                    }
                }
                
                $gen_mouse_ev_cmd_pipe= $gen_mouse_ev_cmd_pipes[0];
            }
            
            if (!$gen_mouse_ev_cmd_pipe) {
                echo $myname.": Error: popen(".$xdotool_cmd.") failed, using xdotool without pipe.\n";
            } else {
                if ($verbose_mode || $debug_mode) {
                    echo $myname.": Successfully created the pipe to ".XDOTOOL_CMD.".\n";
                }    
                
                // Not effective.
                /*
                $s= stream_set_write_buffer($gen_mouse_ev_cmd_pipe, 0);
                if ($s != 0) {
                    echo $myname.": Error: stream_set_write_buffer failed (s=".$s.").\n";
                }
                */
            }
        }
    }	    
    
    $mouse_fp= FALSE;
    
RESTART:    
    if ($mouse_dev_arg === FALSE) { // Find the mice.
        if ($verbose_mode || $debug_mode) {
            echo $myname.": Finding mice...\n";
        }
        $mouse_devs= find_mice();
        if (!$mouse_devs) {
            echo $myname.": ".date("Y/m/d H:i:s").": Error: No mice found, restarting...\n";
            
            /*
            cleanup();
            
            exit(1);
            */
            
            sleep(RESTART_WAIT_S);            
            
            goto RESTART;
        }	        
       
        // Use the first mouse dev.: Others TODO.
        $mouse_dev= $mouse_devs[0];
        
        if (count($mouse_devs) > 1) {
            echo $myname.": Warning: More than one mice found. Using the first one (".$mouse_devs[0].").\n";
        }        
    } else { // Use specified device.
        $mouse_dev= $mouse_dev_arg;
        
        if (/* $verbose_mode || */ $debug_mode) {
            echo $myname.": Using specified mouse:".$mouse_dev.".\n";
        }

        // $mouse_dev= $argv[$arg_idx];
    }
	if ($debug_mode) {
	    echo $myname.": Using mouse event dev: ".$mouse_dev.".\n";
	}

	// Check the permission.
	if (!is_readable($mouse_dev)) {
	    echo $myname.": Error: Cannot read the mouse event dev: ".$mouse_dev.". Add your read permission to it (eg. add your id to input group).\n";
	    
	    exit(1);
	} else {
        if (/* $verbose_mode || */ $debug_mode) {
            echo $myname.": Can read mouse event dev: ".$mouse_dev.": OK.\n";
        }
	}
	
    if ($verbose_mode) {
        echo $myname.": ".PROG_NAME." V".PROG_VERSION." started. (Wheel acc-mode=".$wheel_acc_mode.", mouse-dev='".$mouse_dev."', gen-ev-cmd=".($use_gen_mouse_ev_cmd ? GEN_MOUSE_EV_CMD_BASENAME:XDOTOOL_CMD).")\n"; 
    }
	
	// exit(0);
	
	if (FALSE) { // Already done.
        // Start and create the pipe to gen-mouse-ev proc.
        $gen_mouse_ev_cmd_pipe= FALSE;
        if ($use_gen_mouse_ev_cmd) {
            if (FALSE) { // Already done.
                // Search gen-mouse-ev command and init.
                $gen_mouse_ev_cmd= FALSE;
                
                $gen_mouse_ev_cmd= trim(shell_exec("which ".GEN_MOUSE_EV_CMD_BASENAME));
                if ($debug_mode) {
                    echo $myname.": which res=".$gen_mouse_ev_cmd."\n";
                }
                
                if (!$gen_mouse_ev_cmd) {
                    foreach ($gen_mouse_ev_cmd_cands as $cmd_path) {
                        if (is_executable($cmd_path)) {
                            $gen_mouse_ev_cmd= $cmd_path;
        
                            if ($debug_mode) {
                                echo $myname.": Found: gen_mouse_ev_cmd=".$gen_mouse_ev_cmd."\n";
                            }                    
                            
                            break;
                        }
                    }
                }
                
                if ($debug_mode) {
                    echo $myname.": gen_mouse_ev_cmd=".$gen_mouse_ev_cmd."\n";
                }
                        
            }
    
            if (!$gen_mouse_ev_cmd) { // Should never haeepen. Just in case.
                echo $myname.": Error: ".GEN_MOUSE_EV_CMD_BASENAME." not found or not executable.\n";
                
                exit(1);
            }
            
            if ($debug_mode) {
                echo $myname.": Opening pipe to ".GEN_MOUSE_EV_CMD_BASENAME."...\n";
            }                            
            $gen_mouse_ev_cmd_pipe= popen($gen_mouse_ev_cmd, "w");
            if (!$gen_mouse_ev_cmd_pipe) {
                echo $myname.": Error: popen(".$gen_mouse_ev_cmd.") failed, using xdotool.\n";
            } else {
                if ($verbose_mode || $debug_mode) {
                    echo $myname.": Successfully created the pipe to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
                }                    
            }
        } else { // xdotool
            if (USE_XDOTOOL_VIA_FIFO) {
                $xdt_fifo_name= "/tmp/aw-xdt-".$uname.".fifo"; // TODO
                
                if (file_exists($xdt_fifo_name)) {
                    @unlink($xdt_fifo_name);
                }
                
                $cmd= "mkfifo ".$xdt_fifo_name;
                if (TRUE || $debug_mode) {
                    echo $myname.": Making named pipe ".$xdt_fifo_name." for ".XDOTOOL_CMD." (cmd=".$cmd.")...\n";
                }
                
                $ret_str= system($cmd, $ret);
                if ($ret != 0) {
                    echo $myname.": Error: Make named pipe for ".XDOTOOL_CMD." (cmd=".$cmd.") failed (ret=".$ret.", ret_str=".$ret_str.").\n";
                    
                    // TODO
                    
                    exit(1);
                } else {
                    if (TRUE || $debug_mode) {
                        echo $myname.": Successfully made named pipe for ".XDOTOOL_CMD.".\n";
                    }
                    
                    $cmd= XDOTOOL_CMD." ".$xdt_fifo_name." < /dev/null > /dev/null 2>&1 &";
                    $ret_str= system($cmd, $ret);
                    if ($ret != 0) {
                        echo $myname.": Error: Failed to start ".XDOTOOL_CMD."  (ret=".$ret.", ret_str=".$ret_str.", cmd=".$cmd.").\n";
                        
                        // TODO
                        
                        exit(1);
                    } else {            
                        if ($verbose_mode || $debug_mode) {
                            echo $myname.": Successfully started ".XDOTOOL_CMD.".\n";
                        }                    
    
                        $gen_mouse_ev_cmd_pipe= fopen($xdt_fifo_name, "w");
                        if (!$gen_mouse_ev_cmd_pipe) {
                            echo $myname.": Error: Failed to open named pipe ".$xdt_fifo_name." for ".XDOTOOL_CMD.".\n";
        
                            // TODO
                        
                            exit(1);
                        } else {
                            if ($verbose_mode || $debug_mode) {
                                echo $myname.": Successfully opened named pipe for ".XDOTOOL_CMD.".\n";
                            }                    
                        }
                    }
                }        
            } else if (USE_XDOTOOL_VIA_PIPE) { // Use xdotool via pipe.
                $xdotool_cmd= XDOTOOL_CMD." -";            
                // $xdotool_cmd= "exec ".XDOTOOL_CMD." -";            
                if ($debug_mode) {
                    // $xdotool_cmd= "cat | ".XDOTOOL_CMD." -";
                    // $xdotool_cmd= "tee /tmp/aw-xdt-cmds.txt | ".XDOTOOL_CMD." -";
                    // $xdotool_cmd.= " > /tmp/aw-xdt.log 2>&1";
                    
                    echo $myname.": Opening pipe to ".XDOTOOL_CMD." (cmd=".$xdotool_cmd.")...\n";
                }                            
                   
                $gen_mouse_ev_cmd_pipe= popen($xdotool_cmd, "w");
                if (FALSE) { // Trying with proc_open (same result as popen).
                    $desc_spec= array(
                        0=>array("pipe", "r") // stdin of child.
                    );
                    $gen_mouse_ev_cmd_pipes= FALSE;
                    $xdt_proc= proc_open($xdotool_cmd, $desc_spec , $gen_mouse_ev_cmd_pipes);
                    
                    if (!is_resource($xdt_proc)) {
                        echo $myname.": Error: proc_open(".$xdotool_cmd.") failed.\n";
                        
                        // TODO
                        
                        exit(1);
                    } else {
                        if ($debug_mode) {
                            echo $myname.": gen_mouse_ev_cmd_pipes=".print_r($gen_mouse_ev_cmd_pipes, TRUE).".\n";
                        }
                    }
                    
                    $gen_mouse_ev_cmd_pipe= $gen_mouse_ev_cmd_pipes[0];
                }
                
                if (!$gen_mouse_ev_cmd_pipe) {
                    echo $myname.": Error: popen(".$xdotool_cmd.") failed, using xdotool without pipe.\n";
                } else {
                    if ($verbose_mode || $debug_mode) {
                        echo $myname.": Successfully created the pipe to ".XDOTOOL_CMD.".\n";
                    }    
                    
                    // Not effective.
                    /*
                    $s= stream_set_write_buffer($gen_mouse_ev_cmd_pipe, 0);
                    if ($s != 0) {
                        echo $myname.": Error: stream_set_write_buffer failed (s=".$s.").\n";
                    }
                    */
                }
            }
        }
    } // End Already done.
	
// RESTART: // Must find the mouse again.
	// (Re-)Open the mouse.
	if ($mouse_fp) {
	    fclose($mouse_fp);
	}
	$mouse_fp= fopen($mouse_dev, "r");
	if (!$mouse_fp) {
		echo $myname.": ".date("Y/m/d H:i:s").": Error: Failed to open ".$mouse_dev.", restarting...\n";
		
		sleep(RESTART_WAIT_S);
		
		goto RESTART;
		
		/*
		cleanup();
		
		exit(1);
		*/
	}

	$tmp_read_ary= array();
    // $perform_accel= FALSE;
	while (TRUE) {
	    // Read all events at once.
        if ($debug_v_mode) {
            echo $myname.": Reading events...\n";
        }
	    $mouse_ev_data= FALSE;
	    while (TRUE) {
            // Wait for events.
            {
                // $tmp_read_ary= array($mouse_fp);
                $tmp_read_ary[0]= $mouse_fp;
                $tmp_write_ary= NULL;
                $tmp_except_ary= NULL;
                $s= @stream_select($tmp_read_ary, $tmp_write_ary, $tmp_except_ary, 0, EV_WAIT_TIME_MAX_MS*1000);
                pcntl_signal_dispatch(); // Somehow, needed to handle interrupts.
                if (!$s) {
                    if ($s === FALSE) { // Possible interrupt.
                        echo "main: stream_select() error, possible interrupt.\n";
                        
                        cleanup();
                        exit(0);
                    }
                    
                    if ($debug_v_mode) {
                        echo $myname.": No events any more, processing read events...\n";
                    }
                    
                    break;
                }
            }
                
            if ($debug_v_mode) {
                echo "read fds="; print_r($read);
                echo "except fds="; print_r($except);
                
                echo "Reading events...\n";
            }
            
            // $rem_str= FALSE;
            // Read all events at once.
            if ($debug_v_mode) {
                echo $myname.": Receiving a packet...";
            }
            
            // $mouse_ev_data.= stream_get_contents($mouse_fp, -1); // Never returns.
            $tmp_data= fread($mouse_fp, EVDEV_EV_PKT_LEN);
            
            if ($debug_v_mode) {
                echo " done, len=".strlen($tmp_data).".\n";
            //    print_r($rem_str);
            }
    
            if (!$tmp_data) {
                echo $myname.": ".date("Y/m/d H:i:s").": Error: Failed to read event, restarting...\n";
                 
                fclose($mouse_fp);
                $mouse_fp= FALSE;
                
                sleep(RESTART_WAIT_S);
                
                // break;
                goto RESTART;
            }
            
            $mouse_ev_data.= $tmp_data;
        }

		if ($debug_v_mode && $mouse_ev_data) {
            echo $myname.": Processing read events (len=".strlen($mouse_ev_data).")...\n";

            if (FALSE) {
                echo "[";
                for ($i= 0; $i < EVDEV_EV_PKT_LEN; $i++) {
                    $tmp= substr($mouse_ev_data, $i, 1);
                    $ary= unpack("C", $tmp);
                    $d= $ary[1];
                    printf("0x%x ", $d);
                }
                echo "]\n";
			}
		}
		
		// Handle read packets.
		$idx= 0;
		$prev_ev_t= FALSE;
		$prev_wheel_dir= FALSE;
		$num_wheel_evs= 0;
		$ev_beg_t= FALSE; // WHEEL_AGGR_ACCEL
		$ev_end_t= FALSE; // WHEEL_AGGR_ACCEL
		while (TRUE) {
		    $ev_packet= substr($mouse_ev_data, $idx, EVDEV_EV_PKT_LEN);
		    if (!$ev_packet) {
		        break;
		    }
		    
		    if ($debug_v_mode) {
		        echo $myname.": Processing packet at ".$idx."...\n";
		    }
		    
		    if (($pkt_len= strlen($ev_packet)) != EVDEV_EV_PKT_LEN) {
		        echo $myname.": Ignoring short packet (len=".$pkt_len.").\n";
		        
		        break;
		    }
		    $idx+= $pkt_len;
		    
            $flush_ev= FALSE;
            $flush_ev_reason= FALSE;
            
		    // Check the packet format, parse event data and obtain mouse event.
            $ev_info= unpack_ev_data($ev_packet);
            if ($debug_v_mode) {
                echo $myname.": ev_info="; print_r($ev_info);
            }
            // Ignore "Syn" events.
            if (ev_is_syn($ev_info)) {
                if ($debug_v_mode) {
                    echo $myname.": Discarding SYN event.\n";
                }
            } else {            
                $wheel_info= ext_mouse_ev($ev_info);
                if (!$wheel_info) { // Other event.
                    if ($debug_v_mode) {
                        $ev_info= unpack_ev_data($ev_packet);
                        
                        printf("%s: %d.%06d: Other event(0x%x, 0x%x, 0x%x) received.\n", $myname, $ev_info["time_h"],$ev_info["time_l"],  $ev_info["type"], $ev_info["code"], $ev_info["value"]);
                                
                        if (FALSE) {
                            echo "[";
                            for ($i= 0; $i < EVDEV_EV_PKT_LEN; $i++) {
                                $tmp= substr($ev_packet, $i, 1);
                                $ary= unpack("C", $tmp);
                                $d= $ary[1];
                                printf("0x%x ", $d);
                            }
                            echo "]\n";
                        }
                    }
                    
                    if (WHEEL_FLUSH_EV_EC) {
                        if ($debug_mode) {
                            echo $myname.": Event changed to non wheel (type=0x".$ev_info["type"].", code=0x".$ev_info["code"]."), flush will be performed.\n";
                        }
                                
                        $flush_ev= TRUE;
                        $flush_ev_reason= "EC";
                    }
                } else { // Wheel event.            
                    $ev_t= $wheel_info["time"];
                    $wheel_dir= $wheel_info["dir"];
                    $wheel_val= $wheel_info["val"];
                    
                    $flush_ev= FALSE;
                    $flush_ev_reason= FALSE;
                    
                    if ($wheel_dir == $prev_wheel_dir) { // Continuous wheel event.
                        $dt_ms= ($ev_t - $prev_ev_t)*1000; // -> ms.
                        $num_wheel_evs++;
                    
                        if (WHEEL_AGGR_ACCEL) {
                            $ev_end_t= $ev_t;
                        }
                              
                        /*
                        if ($debug_mode) {
                            if ($dt_ms <= WHEEL_ACCEL_DT_MS) {
                                $t= microtime(TRUE);
                                
                                echo $myname.": ".$t.": short int (#".$num_wheel_evs.").\n";
                            }
                            
                            // $perform_accel= TRUE;
                        }
                        */
                    } else { 
                        if ($prev_wheel_dir) { // Wheel rotation dir. changed.
                            $flush_ev= TRUE;
                            $flush_ev_reason= "DC";
                            
                            if ($debug_mode) {
                                echo "Dir. changed: ".$prev_wheel_dir." -> ".$wheel_dir.".\n";
                            }
                        }
                    }

                    if (WHEEL_AGGR_ACCEL && !$ev_beg_t) {
                        $ev_beg_t= $ev_t;
                    }       
                    
                    if (WHEEL_AGGR_ACCEL && defined("WHEEL_AGGR_ACCEL_MAX_MS") && $num_wheel_evs > 1 && $ev_t && $ev_beg_t) {
                        $tmp_dt_ms= ($ev_t - $ev_beg_t)*1000;
                        
                        if ($tmp_dt_ms >= WHEEL_AGGR_ACCEL_MAX_MS) {
                            $flush_ev= TRUE;
                            $flush_ev_reason= "TO";
                            
                            $dt_ms= $tmp_dt_ms;
                        }
                    }
                    
                    $prev_ev_t= $ev_t;
                    $prev_wheel_dir= $wheel_dir;
                }
            }
            
            if (!WHEEL_AGGR_ACCEL || $flush_ev) {
                /* if ($perform_accel ) */ {
                    if ($num_wheel_evs > 1) { // Issue additional wheel events.
                        // Note: If $num_wheel_evs is 1, no accel. will be performed actually because wheel event speed (vw) cannot be calculated.
/*                        
                        if ($debug_mode) {
                            echo $myname.": perform_accel: num_wheel_evs=".$num_wheel_evs.".\n";
                        }
*/
                        if (!$dt_ms) {
                            if ($debug_mode) {
                                echo $myname.": Calc. dt_ms.\n";
                            }
                            
                            $dt_ms= ($ev_t - $ev_beg_t)*1000;
                        }

                        if ($debug_mode) {
                            echo $myname.": Flushing evs. (reason=".$flush_ev_reason.", dt_ms=".$dt_ms."(ms), num_wheel_evs=$num_wheel_evs).\n";
                        }

                        $s= issue_add_wheel_events($wheel_dir, $num_wheel_evs, $dt_ms, $use_gen_mouse_ev_cmd, $gen_mouse_ev_cmd_pipe, $wheel_acc_mode);
                        
                        $num_wheel_evs= 0;     
                        // $perform_accel= FALSE;
                        if (WHEEL_AGGR_ACCEL) {
                            $ev_end_t= $ev_beg_t= FALSE;
                        }
                        $flush_ev= FALSE;
                        $flush_ev_reason= FALSE;                        
                        $dt_ms= FALSE;
                    }                        
                }
            }

		    // exit(0);
		}
		
        if ($debug_v_mode) {
            echo $myname.": Finished processing packets.\n";
        }

// NEXT:
        if (WHEEL_AGGR_ACCEL) { // Perform accel. if needed.
            if ($debug_mode) {
                if ($num_wheel_evs > 0 || $debug_v_mode) {
                    echo $myname.": num_wheel_evs=".$num_wheel_evs.".\n";
                }
            }
            if ($num_wheel_evs > 0) {
                if ($debug_mode) {
                    echo $myname.": perform_accel: num_wheel_evs=".$num_wheel_evs.".\n";
                }
                
                if ($num_wheel_evs > 1) { // Issue additional wheel events.
                    // Note: If $num_wheel_evs is 1, no accel. will be performed actually because wheel event speed (vw) cannot be calculated.
                    
                    if (!$ev_end_t || !$ev_beg_t) {
                        echo $myname.": perform_accel: Internal error: ev_beg_t or ev_end_t not valid.\n"; 
                    } else {                        
                        $dt_ms= ($ev_end_t - $ev_beg_t)*1000; // sec. -> ms.
                        $s= issue_add_wheel_events($wheel_dir, $num_wheel_evs, $dt_ms, $use_gen_mouse_ev_cmd, $gen_mouse_ev_cmd_pipe, $wheel_acc_mode);
                        
                        $ev_end_t= $ev_beg_t= FALSE;
                    }
                }
                
                $num_wheel_evs= 0;
                // $perform_accel= FALSE;
            }
        }
	}

	// Not reached.
    cleanup();
	
	exit(0);
}

// Find mouse event devs ("/dev/input/eventX").
function find_mice()
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;

    if ($debug_mode) {
        echo "find_mice: Called.\n";
    }

    $cmd= "(cd /dev/input/by-id && ls -l *-event-mouse | awk '{print $(NF);}' | xargs realpath ) 2>/dev/null";    
    $mice_str= trim(shell_exec($cmd));
    
    if ($debug_mode) {
        echo "find_mice: mice_str='".$mice_str."'\n";
    }
    
    if ($mice_str=="") { // No mice.
        if ($debug_mode) {
            echo "find_mice: No mice found.\n";
        }
        
        return FALSE;
    } else {
        $mice_array= explode("\n", $mice_str);
        
        if ($debug_mode) {
            echo "find_mice: mice_array="; print_r($mice_array);
        }
        
        return $mice_array;
    }
    
    return FALSE;
}

// Unpack event packet.
// Check the packet format, too.
function unpack_ev_data($ev_packet)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;

    // Check the packet format.: TODO
    
    /*
        struct input_event {
        struct timeval time; // 16B
        __u16 type; // 2B 
        __u16 code; // 2B 
        __s32 value; // 4B
        };
    */
    $ev_info= unpack("Ptime_h/Ptime_l/vtype/vcode/Vvalue", $ev_packet);
    
    return $ev_info;
}

// Check if specified event is a Sync event.
function ev_is_syn($ev_info)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;

    return $ev_info["type"] == EV_TYPE_SYN && $ev_info["code"] == EV_CODE_SYN_SYN;
}

// Extract mouse event from an event info.
// Returns: Array ("time"=>time, "dir"=>wheel_dir, "val"=>wheel_val).
function ext_mouse_ev($ev_info)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;

    /*
    $ev_info= unpack_ev_data($ev_packet);
    if ($debug_mode) {
        echo "ext_mouse_ev: ev_info="; print_r($ev_info);
    }
    */
    
    if ($ev_info && $ev_info["type"] == EV_TYPE_REL 
        && $ev_info["code"] == EV_CODE_REL_WHEEL) {
        $wheel_dir= FALSE;
        switch ($ev_info["value"]) {
            case EV_VAL_WHEEL_UP:
                $ev_val_str= "Up";
                $wheel_dir= $ev_val_str; // $ev_info["value"];
                break;
                
            case EV_VAL_WHEEL_DOWN:
                $ev_val_str= "Down";
                $wheel_dir= $ev_val_str; // $ev_info["value"];
                break;
                
            default:
                echo "ext_mouse_ev: Warning: Unsupported event val. 0x".$ev_info["value"]." ignored.\n";
                
                $ev_val_str= "?";
                break;
        }	                
        
        if ($debug_mode) {
            printf("ext_mouse_ev: %d.%06d: Detected mouse wheel event: %s (0x%x).\n", $ev_info["time_h"], $ev_info["time_l"], $ev_val_str, $ev_info["value"]);
        }
        
        $ev_t= $ev_info["time_h"] + $ev_info["time_l"]/1000000;
        
        $res= array("time"=>$ev_t, "dir"=>$wheel_dir, "val"=>$ev_info["value"]);
        
        return $res;
    }
    
    return FALSE;
}

// Calcurate accelerated wheel events.
function calc_accel_wheel_evs($num_wheel_evs, $dt_ms, $wheel_acc_mode= DEF_WHEEL_ACCEL_MODE)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;
    global $wheel_acc_configs;
    
    if ($num_wheel_evs <= 1) {
        return 0; // No add. events. // $num_wheel_evs;
    }
    
    $wheel_acc_config= $wheel_acc_configs[$wheel_acc_mode];
    
    $vw= $num_wheel_evs/$dt_ms; // Velocity of wheel (evs/ms).
    if ($wheel_acc_mode == "LS" || $wheel_acc_mode == "L") {
        if ($vw < $wheel_acc_config["WHEEL_ACCEL_VW_MIN"] || $vw < $wheel_acc_config["WHEEL_ACCEL_VW_SUB"]) {
            if ($debug_mode) {
                echo "calc_accel_wheel_evs: [L/LS] Small vw ".$vw." (ev/ms) ignored.\n";
            }
            
            return 0;
        }
        
        $k= $wheel_acc_config["WHEEL_ACCEL_VW_MULT"] * ($vw - $wheel_acc_config["WHEEL_ACCEL_VW_SUB"]) + $wheel_acc_config["WHEEL_ACCEL_MULT_OFF"];
        
        $num_new_wheel_evs= ceil($k * $num_wheel_evs);
    } else if ($wheel_acc_mode == "S") {
        if ($vw >= $wheel_acc_config["WHEEL_ACCEL_VW_TH"]) {
            if ($debug_mode) {
                echo "calc_accel_wheel_evs: [S] Large vw=".$vw." (ev/ms), increasing accel.\n";
            } else if ($verbose_mode) {
                echo "calc_accel_wheel_evs: [S] Inc-acc.\n";
            }
            
            $k= $wheel_acc_config["WHEEL_ACCEL_A"] * $vw / $wheel_acc_config["WHEEL_ACCEL_VW_TH"];
        } else {
            $k= 1;
        }
        $num_new_wheel_evs= ceil($k * $num_wheel_evs * $wheel_acc_config["WHEEL_ACCEL_VW_MULT"]);        
    }
        
    $num_add_wheel_evs= $num_new_wheel_evs - $num_wheel_evs; 
    $num_add_wheel_evs= max(0, $num_add_wheel_evs); 
    if (isset($wheel_acc_config["WHEEL_EV_ADD_MAX"])) { // Limit the mun of events.
        $num_add_wheel_evs= min($wheel_acc_config["WHEEL_EV_ADD_MAX"], $num_add_wheel_evs);
        // $num_new_wheel_evs= min(WHEEL_EV_ADD_MAX, $num_new_wheel_evs); // Wrong!
    }
    $num_add_wheel_evs= floor($num_add_wheel_evs);
    
    if ($debug_mode) {
        printf("calc_accel_wheel_evs: [".$wheel_acc_mode."] num_wheel_evs=%d, dt=%.1f(ms) -> vw=%.3f (ev/ms) -> k=%.2f -> num_new_wheel_evs=%d (add %d) (WHEEL_EV_ADD_MAX=%d).\n", $num_wheel_evs, $dt_ms, $vw, $k, $num_new_wheel_evs, $num_add_wheel_evs, $wheel_acc_config["WHEEL_EV_ADD_MAX"]);
    }
        
    return $num_add_wheel_evs;
}

// Issue mouse wheel events.
function issue_wheel_events($dir, $num= 1, $use_gen_mouse_ev_cmd= TRUE, $gen_mouse_ev_pipe= FALSE /* for gen-mouse-ev(when $use_gen_mouse_ev_cmd) or xdotool(when USE_XDOTOOL_VIA_PIPE) */, $ev_add_int_ms= FALSE)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;

    if ($debug_mode) {
        echo "issue_wheel_events: dir=".$dir.", num=".$num.".\n";
        echo "issue_wheel_events: ev_add_int_ms=".$ev_add_int_ms.".\n";
    }

    $btn= ($dir == "Up" ? "4":"5");
    
    if (defined("WHEEL_ACCEL_DELAY_MS")) {
        if ($debug_mode) {
            echo "issue_wheel_events: WHEEL_ACCEL_DELAY_MS.\n";
        }
        
        usleep(WHEEL_ACCEL_DELAY_MS*1000);
    }

    if ($use_gen_mouse_ev_cmd && !$gen_mouse_ev_pipe) {
        echo "issue_wheel_events: Internal error: gen_mouse_ev_pipe not valid.\n";
        
        return FALSE;
    }
    
    if ($use_gen_mouse_ev_cmd) {        
        if ($num > 1 && $ev_add_int_ms !== FALSE /* defined("WHEEL_EV_ADD_INT_MS") */) {
            $delay_sym= "\n"."S".$ev_add_int_ms;
//            $delay_sym= "\n"."S".WHEEL_EV_ADD_INT_MS;
        } else {
            $delay_sym= "";
        }
        
        if ($ev_add_int_ms !== FALSE /* defined("WHEEL_EV_ADD_INT_MS")*/ && $debug_mode) {
            echo "issue_wheel_events: ev_add_int_ms=".$ev_add_int_ms.", delay_sym='".$delay_sym."'.\n";
//            echo "issue_wheel_events: WHEEL_EV_ADD_INT_MS=".WHEEL_EV_ADD_INT_MS.", delay_sym='".$delay_sym."'.\n";
        }
        
        if (GEN_MOUSE_EV_CMD_PRE_SLEEP_US) {
            if ($debug_mode) {
                echo "issue_wheel_events: Sleeping ".(GEN_MOUSE_EV_CMD_PRE_SLEEP_US/1000)." (ms) before generating add. events.\n";
            }        
            
            usleep(GEN_MOUSE_EV_CMD_PRE_SLEEP_US);
        }
        
        if (GEN_MOUSE_EV_CMD_PRE_XSYNC) { // Issue XSync before generating add. events.
            if ($debug_mode) {
                echo "issue_wheel_events: GEN_MOUSE_EV_CMD_PRE_XSYNC: Sending newline to issue XSync.\n";
            }
            $s= fprintf($gen_mouse_ev_pipe, "\n");
            if (!$s) {                    
                echo "issue_wheel_events: Error: Failed to send newline to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
                
                return FALSE;
            }
            fflush($gen_mouse_ev_pipe);
        }
                
        for ($i= 0; $i < $num; $i++) {
            $send_str= sprintf("%s%s", $btn, $delay_sym);
            
            if ($debug_mode) {
                echo "issue_wheel_events: Sending str '".$send_str."' to ".GEN_MOUSE_EV_CMD_BASENAME."\n";
            }
            
            // $s= fprintf($gen_mouse_ev_pipe, "%s%s\n", $btn, $delay_sym);
            
            $s= fprintf($gen_mouse_ev_pipe, "%s\n", $send_str);
            if (!$s) {
                echo "issue_wheel_events: Error: Failed to send str '".$send_str."' to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
                
                break;
            }
            
            if (defined("GEN_MOUSE_EV_CMD_XSYNC_EV_INT") 
                && (($i % GEN_MOUSE_EV_CMD_XSYNC_EV_INT) == (GEN_MOUSE_EV_CMD_XSYNC_EV_INT -1))) { // Send a newline and perform XSync.
                if ($debug_mode) {
                    echo "issue_wheel_events: Sending newline to perform XSync (i=".$i.").'\n";
                }
                $s= fprintf($gen_mouse_ev_pipe, "\n");
                if (!$s) {                    
                    echo "issue_wheel_events: Error: Failed to send newline to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
                    
                    break;
                }
                fflush($gen_mouse_ev_pipe);
                
                if (defined("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS")) {
                    usleep(GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS*1000);
                }
            }
        }
        
        if ($s) {
            // Send a newline and perform XSync.
            $s= fprintf($gen_mouse_ev_pipe, "\n");
            if (!$s) {
                echo "issue_wheel_events: Error: Failed to send newline to ".GEN_MOUSE_EV_CMD_BASENAME.".\n";
                
                return FALSE;
            }
        }
        
        fflush($gen_mouse_ev_pipe);
        
        // Check for xdotool error.: TODO
        // Note: Error can be detected next time (fprintf will be error).
        
        if (defined("GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS")) {
            usleep(GEN_MOUSE_EV_CMD_XSYNC_DELAY_MS*1000);
        }
        
        return $s;
    } else { // xdotool
        $delay_arg= "";
        $rep_arg= "";
        $sleep_cmd= "";
        if ($num > 1) {
            if ($ev_add_int_ms !== FALSE /* defined("WHEEL_EV_ADD_INT_MS")*/) { 
                $delay_arg= " --delay ".$ev_add_int_ms; // ." ";
//                $delay_arg= " --delay ".WHEEL_EV_ADD_INT_MS." ";
            } /* else {
                $delay_arg= "";
            } */
            if (XDOTOOL_NO_REPEAT) {
                $sleep_cmd= " sleep ".($ev_add_int_ms/1000);
            } else {
                $rep_arg= "--repeat ".$num.$delay_arg." ";
            }
        }
        
        if (XDOTOOL_ISSUE_EV_SPECIFY_WIN) {
            $search_win_arg= "getwindowfocus ";
            $click_arg= "--window %1 ";
        } else {
            $click_arg= $search_win_arg= "";
        }
        
        if ($gen_mouse_ev_pipe) { // Send xdotool commands via pipe.
            if (XDOTOOL_NO_REPEAT) {
                $num_write= $num;
            } else {
                $num_write= 1;
            }
            $pipe_cmd= $search_win_arg."click ".$click_arg.$rep_arg.$btn.$sleep_cmd; // ." ";
                
            if ($debug_mode) {
                /*
                // Add debug msg.
                $dbg_msg= "exec echo -n '".date("Y/m/d H:i:s").": '\n";                
                $pipe_cmd= $dbg_msg.$pipe_cmd;
                */
                
                echo "issue_wheel_events: pipe_cmd='".$pipe_cmd."'\n";
            }
            
            for ($cnt= 0; $cnt < $num_write; $cnt++) {
                if ($debug_v_mode) {
                    echo "issue_wheel_events: XDOTOOL_NO_REPEAT: Write ".$cnt."/".$num_write.".\n";
                }
                
                // $s= fprintf($gen_mouse_ev_pipe, "%s", $pipe_cmd);
                // $s= fprintf($gen_mouse_ev_pipe, "%s\n", $pipe_cmd);
                $s= fwrite($gen_mouse_ev_pipe, $pipe_cmd."\n");
                if (!$s) {
                    echo "issue_wheel_events: Error: Failed to send commands '".$str."' to ".XDOTOOL_CMD.".\n";
                    
                    return FALSE;
                } else if ($debug_v_mode) {
                    echo "issue_wheel_events: fprintf/fwrite returned ".$s.".\n";
                }
                fflush($gen_mouse_ev_pipe);
                
                /*
                if (XDOTOOL_NO_REPEAT && $ev_add_int_ms !== FALSE) {
                    if (TRUE || $debug_v_mode) {
                        echo "issue_wheel_events: sleep ".$ev_add_int_ms."(ms).\n";
                    }
                    usleep($ev_add_int_ms*1000);
                }
                */
            }
            
            // fflush($gen_mouse_ev_pipe);
            
            return TRUE;
        } else {
            $cmd= "xdotool ".$search_win_arg." click ".$click_arg.$rep_arg." ".$btn;
        
            if ($debug_mode) {
                echo "issue_wheel_events: cmd='".$cmd."'\n";
            }
        
            $str= system($cmd, $ret);

            return $ret==0;
        }        
    }
    
    // Not reached.
    return FALSE;
}

// Issue accel. mouse wheel events of $wheel_dir if needed (according to latest wheel events status: $num_wheel_evs, $dt_ms).
function issue_add_wheel_events($wheel_dir, $num_wheel_evs, $dt_ms, $use_gen_mouse_ev_cmd= TRUE, $gen_mouse_ev_pipe= FALSE /* for gen-mouse-ev(when $use_gen_mouse_ev_cmd) or xdotool(when USE_XDOTOOL_VIA_PIPE) */, $wheel_acc_mode= DEF_WHEEL_ACCEL_MODE)
{
    global $myname;
    global $debug_mode, $debug_v_mode, $verbose_mode;
    global $wheel_acc_configs;
    
    $num_add_wheel_evs= calc_accel_wheel_evs($num_wheel_evs, $dt_ms, $wheel_acc_mode);
    if ($num_add_wheel_evs <= 0) {
        return TRUE;
    }
    
    if ($debug_v_mode) {
        echo $myname.": issue_add_wheel_events: use_gen_mouse_ev_cmd=".($use_gen_mouse_ev_cmd?"Y":"N").".\n";                       
    }           
    
    if ($verbose_mode || $debug_mode) {
        $t= microtime(TRUE); 
        echo $myname.": ".$t.": issue_add_wheel_events: [".($use_gen_mouse_ev_cmd?"GME":"XDT")."][".$wheel_acc_mode."] Issuing ".$num_add_wheel_evs." add. ".$wheel_dir." event(s) (orig. ".$num_wheel_evs.").\n";                       
    }                   
    
    $ev_add_int_ms= FALSE;
    if (defined("WHEEL_EV_ADD_INT_MS")) {
        $ev_add_int_ms= WHEEL_EV_ADD_INT_MS;
    }
    /*
    if (isset($wheel_acc_configs[$wheel_acc_mode]["WHEEL_EV_ADD_INT_MS"])) {
        $ev_add_int_ms= $wheel_acc_configs[$wheel_acc_mode]["WHEEL_EV_ADD_INT_MS"];
    }
    */
    
    $s= issue_wheel_events($wheel_dir, $num_add_wheel_evs, $use_gen_mouse_ev_cmd, $gen_mouse_ev_pipe, $ev_add_int_ms);
    if (!$s) {
        echo $myname.": Error: issue_wheel_events() failed.\n";
    }
    
    return $s;
}

?>