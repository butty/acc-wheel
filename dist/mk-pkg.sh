#!/bin/sh
# Make acc-wheel's package.
# 2020/4/13 butty
# usage: mk-pkg.sh dest-dir pkg-base-name
#   Eg.: ./mk-pkg.sh 20200413 acc-wheel-0.1

if [ $# -lt 2 ]; then
    echo "usage: $0 dest-dir pkg-base-name"
    
    exit 1
fi

# pkg_name="acc-wheel-0.1.tar"

src_dir=..

dest_dir="$1"
pkg_base_name="$2"

pkg_name="$pkg_base_name.tar"

dest_top_dir="acc-wheel"
# dest_top_dir="$dest_dir/acc-wheel"

dist_files="acc-wheel.php gen-mouse-ev/gen-mouse-ev.c"

if [ ! -d "$dest_dir" ]; then 
    if ! mkdir "$dest_dir"; then
        echo "$0: Error: Failed to make $dest_dir"
        
        exit 1
    fi
else
    echo "$0: Error: $dest_dir exists."
    
    exit 1
fi

if false; then
if ! cd "$dest_dir" ; then
    echo "$0: Error: Failed to chdir to $dest_dir"
    
    exit 1
fi
fi

# Make top dir.
if ! mkdir "$dest_dir/$dest_top_dir" ; then
    echo "$0: Error: Failed to make $dest_dir/$dest_top_dir"
    
    exit 1
fi

# Copy the contents.
for file in $dist_files ; do
    if ! cp -p "$src_dir"/$file "$dest_dir/$dest_top_dir" ; then
        echo "$0: Error: Failed to copy $file to $dest_dir/$dest_top_dir"
        
        exit 1
    fi
done

# Make a tar.
(
    if ! cd "$dest_dir"; then
        echo "$0: Error: Failed to chdir $dest_dir"
        
        exit 1
    fi
    
    if ! tar cf "$pkg_name" "$dest_top_dir"; then
        echo "$0: Error: Failed to create pkg $pkg_name"
        
        exit 1
    fi
)
    
echo "done."
