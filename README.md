# acc-wheel

Mouse wheel accelerator for Linux/X11.

acc-wheel implements mouse wheel acceleration as in Windows or macOS on desktop Linux systems running  X Window System (X11). This is not simply multiplying wheel rotation with fixed number (ie. ×2), but a "true acceleration", which increases the amount of rotation according to the speed of rotating speed of the wheel. So, inconvenience never happens in ordinary usage (ie. when rotating the wheel slowly).

In addition, because this is an ordinary program (not special one like device driver), you can use it very handy - just start it.

Notes: Before using, you have to make your user account to be able to read mouse's event device. And, current version requires compilation of an accompanied C program which generates mouse events.


Because documents are not ready, for details (eg. desc. on operation, requirements, supported envs., installation, usage, and demo movies), please refer to [my blog article](https://blog.piulento.net/archives/110091).


[Japanese]

acc-wheelは、X Window System (X11)の動作しているデスクトップLinuxでWindowsやmwillacOSのようなマウスホイールの加速を実現します。これは単にホイールの回転量を常に数倍にする(例: ×2)のではなく、ホイールを回転させる速度に応じて回転量を増やす「真の加速」なので、通常の使用時(= ホイールをゆっくり回す場合)に不都合は起こりません。

また、ドライバなどの特別なプログラムでなく通常のプログラムなので、起動するだけで手軽に使えます。

ただし、実行するユーザーがマウスのイベントデバイスを読み出せるように、あらかじめユーザー情報を設定する必要があります。また、現在の版では、マウスイベントを発生させるためのプログラムのコンパイルが必要です。


ドキュメントは未作成のため、詳細(例: 動作の説明, 要求条件, 動作環境, インストール, 使用手順, デモ動画)については[ブログの投稿](https://blog.piulento.net/archives/110091)をご参照下さい。